import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  URL: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
    this.URL = 'http://localhost:3000';
  }

  register(user) {
    return this.http.post(`${this.URL}/auth/register`, user, this.httpOptions);
  }
}
