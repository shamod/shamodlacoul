import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegistrationService } from './registration.service';
import { UsersService } from '../users/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  registerForm: FormGroup;
  isRegisterFormSubmitted: boolean;

  constructor(public formBuilder: FormBuilder,
              public registerService: RegistrationService,
              public userService: UsersService,
              public route: Router) {
    this.isRegisterFormSubmitted = false;
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(128)]],
      email: ['', [Validators.required, Validators.email, Validators.minLength(1), Validators.maxLength(128)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]],
      repassword: ['', [Validators.required], , Validators.minLength(8), Validators.maxLength(20)],
      }, { validator: this.passwordMatchValidator });
  }

  name() {
    return this.registerForm.get('name');
  }

  email() {
    return this.registerForm.get('email');
  }

  password() {
    return this.registerForm.get('password');
  }

  repassword() {
    return this.registerForm.get('repassword');
  }

  private passwordMatchValidator(formGroup: FormGroup) {
      return formGroup.get('password').value === formGroup.get('repassword').value ? null : {'mismatch': true};
  }

  onSubmitRegister(formGroup: FormGroup) {
    const payload = {
      name: formGroup.get('name').value,
      email: formGroup.get('email').value,
      password: formGroup.get('password').value
    };
    this.isRegisterFormSubmitted = true;
    if (this.registerForm.valid) {
      this.userService.addUser(payload).subscribe(response => {
        console.log(response);
        this.registerForm.reset();
        this.isRegisterFormSubmitted = false;
        this.route.navigate(['login']);
      });
    }
  }

}
