import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  URL: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
    this.URL = 'http://localhost:3000';
  }

  login(credential) {
    return this.http.post(`${this.URL}/user/login`, credential, this.httpOptions);
  }

  isAuthenticated() {
    return !!localStorage.getItem('currentUser');
  }

  isAdmin() {
    return localStorage.getItem('currentUser') && JSON.parse(localStorage.getItem('currentUser')).roles.includes('Admin');
  }

  isUser() {
    return localStorage.getItem('currentUser') && JSON.parse(localStorage.getItem('currentUser')).roles.includes('User');
  }

  getUsername() {
    return localStorage.getItem('currentUser') && JSON.parse(localStorage.getItem('currentUser')).name;
  }

  getRoles() {
    return localStorage.getItem('currentUser') && JSON.parse(localStorage.getItem('currentUser')).roles;
  }
}
