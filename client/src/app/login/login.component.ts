import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isLoginFormSubmitted: boolean;

  constructor(public formBuilder: FormBuilder,
              public loginService: LoginService,
              public router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.minLength(1), Validators.maxLength(128)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]],
    });
  }

  email() {
    return this.loginForm.get('email');
  }

  password() {
    return this.loginForm.get('password');
  }

  onSubmitLogin(formGroup: FormGroup) {
    const payload = {
      email: formGroup.get('email').value,
      password: formGroup.get('password').value
    };
    this.isLoginFormSubmitted = true;
    this.loginService.login(payload).subscribe(response => {
      localStorage.setItem('currentUser', JSON.stringify(response));
      this.isLoginFormSubmitted = false;
      this.loginForm.reset();
      this.router.navigate(['blog']);
    });
  }
}
