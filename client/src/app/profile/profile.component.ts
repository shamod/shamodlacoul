import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users/users.service';
import { LoginService } from '../login/login.service';
import { ProfileService } from './profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  profileObj: Profile;
  isUser: boolean;

  constructor(private readonly userService: UsersService,
              private readonly loginService: LoginService) {
    this.profileObj = {
      photo: '',
      name: '',
      title: '',
      description: '',
      socials: []
    };
    this.isUser = false;
  }

  ngOnInit() {
    this.userService.getUser('5d218f0e786d06c44407ee67').subscribe(response => {
      this.profileObj = Object.assign(this.profileObj, response);
    });
    this.isUser = this.loginService.isUser();
  }

}

interface Profile {
  photo: string;
  name: string;
  title: string;
  description: string;
  socials: Array<Social>;
}

interface Social {
  id: string;
  image: string;
  link: string;
}
