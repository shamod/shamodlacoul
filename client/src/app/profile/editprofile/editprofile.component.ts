import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../../users/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../users/users.interface';

@Component({
    selector: 'app-editprofile',
    templateUrl: './editprofile.template.html',
    styleUrls: ['./editprofile.style.scss']
})
export class EditProfileComponent implements OnInit {

  registerForm: FormGroup;
  isRegisterFormSubmitted: boolean;
  userId: string;

  error: string;
  uploadResponse = { status: '', message: '', filePath: '' };


  constructor(public formBuilder: FormBuilder,
              public userService: UsersService,
              public route: Router,
              public activatedRoute: ActivatedRoute) {
    this.isRegisterFormSubmitted = false;
  }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(128)]],
            photo: ['', [Validators.required]],
            title: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(128)]],
            description: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(128)]],
        });
        this.activatedRoute.queryParams.subscribe(params => {
            if (params.userId) {
                this.userId = params.userId;
                this.userService.getUser(this.userId).subscribe((response: User) => {
                    this.registerForm.controls['name'].setValue(response.name);
                    this.registerForm.controls['title'].setValue(response.title);
                    this.registerForm.controls['description'].setValue(response.description);
                });
            }
        });
    }

    name() {
        return this.registerForm.get('name');
    }

    photo() {
        return this.registerForm.get('photo');
    }

    title() {
        return this.registerForm.get('title');
    }

    description() {
        return this.registerForm.get('description');
    }

    onFileChange(event) {
        if (event.target.files.length) {
          const files = event.target.files[0];
          this.registerForm.get('photo').setValue(files);
        }
      }

    onSubmitRegister(formGroup: FormGroup) {
        const formData = new FormData();
        formData.append('file', this.registerForm.get('photo').value);
        this.userService.upload(formData).subscribe(
            (res) => {
                this.uploadResponse = res;
                if (this.uploadResponse.hasOwnProperty('filepath')) {
                    const payload = {
                        name: formGroup.get('name').value,
                        title: formGroup.get('title').value,
                        description: formGroup.get('description').value,
                        photo: `http://localhost:3000/user/${this.uploadResponse['filepath']}`,
                    };
                    this.isRegisterFormSubmitted = true;
                    if (this.registerForm.valid) {
                    this.userService.editUser(this.userId, payload).subscribe(response => {
                        console.log(response);
                        this.registerForm.reset();
                        this.isRegisterFormSubmitted = false;
                        this.route.navigate(['profile']);
                    });
                    }
                }
            },
            (err) => this.error = err
          );
      }

}
