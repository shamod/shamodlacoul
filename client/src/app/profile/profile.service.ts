import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { User } from '../users/users.interface';

@Injectable()
export class ProfileService {

    URL: string;
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    constructor(private http: HttpClient) {
        this.URL = 'https://mighty-gorge-82792.herokuapp.com';
        this.URL = 'http://localhost:3000';
      }

      getUser(userId: string) {
        return this.http.get(`${this.URL}/user/user/${userId}`);
      }

      editUser(userId: string, user: User) {
        return this.http.put(`${this.URL}/user/edit?userId=${userId}`, user, this.httpOptions);
      }

}
