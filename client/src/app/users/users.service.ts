import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpEventType } from '@angular/common/http';
import { User } from './users.interface';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  URL: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
    this.URL = 'https://mighty-gorge-82792.herokuapp.com';
    this.URL = 'http://localhost:3000';
  }

  getUsers() {
    return this.http.get(`${this.URL}/user/users`);
  }

  getUser(userId: string) {
    return this.http.get(`${this.URL}/user/user/${userId}`);
  }

  addUser(user) {
    return this.http.post(`${this.URL}/user/create`, user, this.httpOptions);
  }

  editUser(userId: string, user: User) {
    return this.http.put(`${this.URL}/user/edit?userId=${userId}`, user, this.httpOptions);
  }

  deleteUser(userId: string) {
    return this.http.delete(`${this.URL}/user/delete?userId=${userId}`, this.httpOptions);
  }

  changePassword(changePasswordObj) {
    return this.http.put(`${this.URL}/user/change-password`, changePasswordObj, this.httpOptions);
  }

  public upload(data) {
    const UPLOAD_URL = `${this.URL}/user/upload`;
    return this.http.post<any>(UPLOAD_URL, data, {
        reportProgress: true,
        observe: 'events'
    }).pipe(map((event) => {
        switch (event.type) {
            case HttpEventType.UploadProgress:
              const progress = Math.round(100 * event.loaded / event.total);
              return { status: 'progress', message: progress };
            case HttpEventType.Response:
              return event.body;
            default:
              return `Unhandled event: ${event.type}`;
          }
    }));
  }
}
