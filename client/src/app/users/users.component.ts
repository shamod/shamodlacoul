import { Component, OnInit } from '@angular/core';
import { Users } from './users.interface';
import { UsersService } from './users.service';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-user',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: Users;

  constructor(public userService: UsersService,
              private readonly loginService: LoginService) { }

  ngOnInit() {
    this.userService.getUsers().subscribe((users: Users) => {
      console.log(users);
      this.users = users;
    });
  }

  onDeleteUser(userId: string) {
    this.userService.deleteUser(userId).subscribe(response => {
      console.log(response);
    });
  }

}
