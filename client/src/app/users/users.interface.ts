export interface User {
    name: string;
    email?: string;
    password?: string;
    title?: string;
    description?: string;
    status?: string;
    roles?: string[];
}

export type Users = Array<User>;
