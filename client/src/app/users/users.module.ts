import { NgModule } from '@angular/core';
import { UsersComponent } from './users.component';
import { CommonModule } from '@angular/common';
import { CreateEditUserComponent } from './createedituser/createedituser.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersService } from './users.service';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        UsersComponent,
        CreateEditUserComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    providers: [UsersService]
})
export class UsersModule { }
