import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../users.service';
import { ActivatedRoute } from '@angular/router';
import { User } from '../users.interface';

@Component({
  selector: 'app-createedituser',
  templateUrl: './createedituser.component.html',
  styleUrls: ['./createedituser.component.scss']
})
export class CreateEditUserComponent implements OnInit {

  registerForm: FormGroup;
  userId: string;

  constructor(public formBuilder: FormBuilder,
              public userService: UsersService,
              public route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      if (params.userId) {
        this.userId = params.userId;
        this.userService.getUser(this.userId).subscribe((response: User) => {
          this.registerForm.controls['name'].setValue(response.name);
          this.registerForm.controls['email'].setValue(response.email);
          this.registerForm.controls['password'].setValue(response.password);
          this.registerForm.controls['status'].setValue(response.status);
          this.registerForm.controls['roles'].setValue(response.roles);
        });
      }
    });

  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      status: ['', [Validators.required]],
      roles: ['', [Validators.required]]
    });
  }

  onSubmitRegister(formGroup: FormGroup) {
    if (this.userId) {
      this.onEditUser(formGroup);
    } else {
      this.onAddUser(formGroup);
    }
  }

  onAddUser(formGroup: FormGroup) {
    const payload = {
      name: formGroup.get('name').value,
      email: formGroup.get('email').value,
      password: formGroup.get('password').value,
      status: formGroup.get('status').value,
      roles: formGroup.get('roles').value
    };
    this.userService.addUser(payload).subscribe(response => {
      console.log(response);
    });
  }

  onEditUser(formGroup: FormGroup) {
    const payload = {
      name: formGroup.get('name').value,
      email: formGroup.get('email').value,
      password: formGroup.get('password').value,
      status: formGroup.get('status').value,
      roles: formGroup.get('roles').value
    };
    console.log(payload);
    if (this.userId) {
      this.userService.editUser(this.userId, payload).subscribe(response => {
        console.log(response);
      });
    }
  }

}
