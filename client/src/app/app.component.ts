import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ProfileComponent } from './profile/profile.component';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { LoginService } from './login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('crissCross', [
      state('criss', style({
        transform: 'rotate(0deg)'
      })),
      state('crossNeg45', style({
        transform: 'rotate(-45deg) translate(-9px, 6px)'
      })),
      state('crossPos45', style({
        transform: 'rotate(45deg) translate(-8px, -8px)'
      })),
      state('hide', style({
        opacity: 0
      })),
      state('show', style({
        opacity: 1
      })),
      transition('criss <=> crossNeg45', [
        animate('0.5s')
      ]),
      transition('criss <=> crossPos45', [
        animate('0.5s')
      ]),
      transition('hide <=> show', [
        animate('0.5s')
      ])
    ])
  ]
})
export class AppComponent implements OnInit {

  profileComponent: ProfileComponent;
  isMobile: boolean;
  showMenu: boolean;

  constructor(private readonly router: Router,
              private readonly loginService: LoginService) {
    this.showMenu = false;
  }

  ngOnInit() {
    this.isMobile = window.matchMedia('(max-width: 768px)').matches;
  }

  onToggleMenu() {
    this.showMenu = !this.showMenu;
  }

  onActivate(component) {
    if (component instanceof ProfileComponent) {
      this.profileComponent = component;
    }
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['/']);
  }
}
