import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component';
import { ContactComponent } from './contact/contact.component';
import { HttpClientModule } from '@angular/common/http';
import { BlogModule } from './blog/blog.module';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { UsersModule } from './users/users.module';
import { CanActivateRouteGuard } from './guards/can-activatre-route.guard';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { VerifyPasswordResetComponent } from './resetpassword/verifypasswordreset/verifypasswordreset.component';
import { ChangePasswordComponent } from './resetpassword/changepassword/changepassword.component';
import { EditProfileComponent } from './profile/editprofile/editprofile.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    EditProfileComponent,
    ContactComponent,
    LoginComponent,
    RegistrationComponent,
    ResetpasswordComponent,
    VerifyPasswordResetComponent,
    ChangePasswordComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BlogModule,
    UsersModule,
  ],
  providers: [CanActivateRouteGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
