import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { map, catchError, flatMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BlogService } from '../blog.service';
import { Blog } from '../blog.interface';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-createblog',
  templateUrl: './createblog.component.html',
  styleUrls: ['./createblog.component.scss']
})
export class CreateblogComponent implements OnInit {

  createBlogForm: FormGroup;
  blogID: string;

  constructor(public formBuilder: FormBuilder,
              public http: HttpClient,
              public blogService: BlogService,
              public route: ActivatedRoute) {}

  ngOnInit() {
    this.buildBlogForm();
    this.route.queryParams.subscribe(params => {
      if (params.postID) {
        this.blogID = params.postID;
        this.blogService.getBlog(this.blogID).subscribe((response: Blog) => {
          this.createBlogForm.controls['title'].setValue(response.title);
          this.createBlogForm.controls['content'].setValue(response.content);
        });
      }
    });
  }

  buildBlogForm() {
    const validationObject = {
      title: ['', [ Validators.required ]],
      content: ['', [ Validators.required ]]
    };
    this.createBlogForm = this.formBuilder.group(validationObject);
  }

  editCreateBlog(formGroup: FormGroup) {
    if (this.blogID) {
      this.onEditBlogFormSubmit(formGroup);
    } else {
      this.onCreateBlogFormSubmit(formGroup);
    }
  }
  onCreateBlogFormSubmit(formGroup: FormGroup) {
    const blog = {
      title: formGroup.get('title').value,
      content: formGroup.get('content').value,
      author: 'Shamod Lacoul',
      date_posted: new Date().toLocaleString()
    };
    this.blogService.addBlog(blog).subscribe((_blog: Blog) => {
      console.log(_blog);
    });
  }

  onEditBlogFormSubmit(formGroup: FormGroup) {
    const blog = {
      title: formGroup.get('title').value,
      content: formGroup.get('content').value,
      author: 'Shamod Lacoul',
      date_posted: new Date().toLocaleString()
    };
    this.blogService.editBlog(this.blogID, blog).subscribe((_blog: Blog) => {
      console.log(_blog);
    });
  }

}
