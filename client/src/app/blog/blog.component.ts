import { Component, OnInit } from '@angular/core';
import { Blogs } from './blog.interface';
import { BlogService } from './blog.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  blogs: Blogs;

  constructor(private blogService: BlogService) {}

  ngOnInit() {
    this.blogService.getBlogs().subscribe((response: Blogs) => {
      this.blogs = response;
    });
  }
}
