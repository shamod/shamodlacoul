import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BlogService } from '../blog.service';

@Component({
    selector: 'app-delete-blog-confirmation',
    templateUrl: './delete-blog-confirmation.template.html'
})
export class DeleteBlogCofirmationComponent {

    @Input() blogID: string;
    @Output() clickNo: EventEmitter<any>;

    constructor(public blogService: BlogService) {
        this.clickNo = new EventEmitter();
    }

    deleteBlog() {
        this.blogService.deleteBlog(this.blogID).subscribe(response => {
            console.log(response);
        });
    }

    closeConfirmDialog() {
        this.clickNo.emit();
    }
}
