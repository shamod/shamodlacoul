import { NgModule } from '@angular/core';
import { CreateblogComponent } from './createblog/createblog.component';
import { BlogComponent } from './blog.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BlogService } from './blog.service';
import { CommonModule } from '@angular/common';
import { DeleteBlogCofirmationComponent } from './delete-blog-confirmation/delete-blog-confirmation.component';
import { RouterModule } from '@angular/router';
import { ViewBlogComponent } from './viewblog/viewblog.component';

@NgModule({
    declarations: [
        BlogComponent,
        CreateblogComponent,
        ViewBlogComponent,
        DeleteBlogCofirmationComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule
    ],
    providers: [BlogService]
})
export class BlogModule { }
