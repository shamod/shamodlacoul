import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Blog } from './blog.interface';
import { Observable } from 'rxjs';

@Injectable()
export class BlogService {

  URL: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
    this.URL = 'https://mighty-gorge-82792.herokuapp.com';
  }

  getBlogs() {
    return this.http.get(`${this.URL}/blog/posts`);
  }

  getBlog(blogID: string) {
    return this.http.get(`${this.URL}/blog/post/${blogID}`);
  }

  addBlog(blog: Blog): Observable<any> {
    return this.http.post(`${this.URL}/blog/post`, blog, this.httpOptions);
  }

  editBlog(blogID: string, blog: Blog) {
    return this.http.put(`${this.URL}/blog/edit?postID=${blogID}`, blog, this.httpOptions);
  }

  deleteBlog(blogID: string) {
    return this.http.delete(`${this.URL}/blog/delete?postID=${blogID}`, this.httpOptions);
  }
}
