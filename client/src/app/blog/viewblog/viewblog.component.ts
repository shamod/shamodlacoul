import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from '../blog.service';
import { Blog } from '../blog.interface';

@Component({
    selector: 'app-viewblog',
    templateUrl: './viewblog.template.html',
    styleUrls: ['./viewblog.style.scss']
})
export class ViewBlogComponent implements OnInit {

    showConfirmDialog: boolean;
    blogID: string;
    blog: Blog;
    isAdmin: boolean;

    constructor(public route: ActivatedRoute,
                public blogService: BlogService) {
        this.showConfirmDialog = false;
        this.isAdmin = false;
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params.id) {
                this.blogID = params.id;
                this.blogService.getBlog(this.blogID).subscribe((response: Blog) => {
                    this.blog = response;
                });
            }
        });
    }

    onOpenDeleteBlogConfirmation() {
        this.showConfirmDialog = true;
    }

    onClickNo() {
        this.showConfirmDialog = false;
    }
}
