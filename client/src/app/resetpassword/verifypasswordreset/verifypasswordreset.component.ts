import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResetPasswordService } from '../resetpassword.service';

@Component({
  selector: 'app-verifypasswordreset',
  templateUrl: './verifypasswordreset.component.html',
  styleUrls: ['./verifypasswordreset.component.scss']
})
export class VerifyPasswordResetComponent implements OnInit {

  isPasswordResetUserVerified: boolean;

  constructor(private readonly activedRoute: ActivatedRoute,
              private readonly resetPasswordService: ResetPasswordService,
              private readonly router: Router) {
    this.isPasswordResetUserVerified = false;
  }

  ngOnInit() {
    this.verifyPasswordResetToken();
  }

  verifyPasswordResetToken() {
    this.activedRoute.params.subscribe(param => {
      if (param.id && param.token) {
        this.resetPasswordService.verifyPasswordResetIdToken({
          id: param.id,
          token: param.token
        }).subscribe((response: any) => {
          if (response.verified) {
            this.isPasswordResetUserVerified = true;
          }
        });
      }
    });
  }
}
