import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyPasswordResetComponent } from './verifypasswordreset.component';

describe('VerifypasswordresetComponent', () => {
  let component: VerifyPasswordResetComponent;
  let fixture: ComponentFixture<VerifyPasswordResetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyPasswordResetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyPasswordResetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
