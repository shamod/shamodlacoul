import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResetPasswordService } from './resetpassword.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

  isResetPasswordFormSubmitted: boolean;
  resetPasswordForm: FormGroup;

  constructor(public formBuilder: FormBuilder,
              public resetPasswordService: ResetPasswordService,
              public router: Router) { }

  ngOnInit() {
    this.resetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.minLength(1), Validators.maxLength(128)]]
    });
  }

  email() {
    return this.resetPasswordForm.get('email');
  }

  onSubmitResetPassword(formGroup: FormGroup) {
    this.isResetPasswordFormSubmitted = true;

    if (this.resetPasswordForm.valid) {
      const payload = {
        email: formGroup.get('email').value
      };
      this.resetPasswordService.verifyEmail(payload).subscribe((response: any) => {
        this.isResetPasswordFormSubmitted = false;
        this.resetPasswordForm.reset();
        this.router.navigate([`login`]);
      });
    }
  }
}
