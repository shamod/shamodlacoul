import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  URL: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
    this.URL = 'http://localhost:3000';
  }

  verifyEmail(email) {
    return this.http.post(`${this.URL}/user/password_reset`, email, this.httpOptions);
  }

  verifyPasswordResetIdToken(passwordReset) {
    console.log(`${this.URL}/user/verify`, passwordReset);
    return this.http.post(`${this.URL}/user/verify_password_reset`, passwordReset, this.httpOptions);
  }
}
