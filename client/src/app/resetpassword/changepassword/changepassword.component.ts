import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/users/users.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  isChangePasswordFormSubmitted: boolean;

  constructor(public formBuilder: FormBuilder,
              public userService: UsersService,
              public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      oldpassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]],
      repassword: ['', [Validators.required], , Validators.minLength(8), Validators.maxLength(20)],
      }, { validator: this.passwordMatchValidator });
  }

  oldpassword() {
    return this.changePasswordForm.get('oldpassword');
  }

  password() {
    return this.changePasswordForm.get('password');
  }

  repassword() {
    return this.changePasswordForm.get('repassword');
  }

  private passwordMatchValidator(formGroup: FormGroup) {
      return formGroup.get('password').value === formGroup.get('repassword').value ? null : {'mismatch': true};
  }

  onSubmitChangePassword(formGroup: FormGroup) {
    this.isChangePasswordFormSubmitted = true;
    if (this.changePasswordForm.valid) {
      this.activatedRoute.params.subscribe(param => {
        const payload = {
          userId: param.id,
          oldpassword: formGroup.get('oldpassword').value,
          password: formGroup.get('password').value
        };
        this.userService.changePassword(payload).subscribe(response => {
          this.changePasswordForm.reset();
          this.isChangePasswordFormSubmitted = false;
        });
      });
    }
  }
}
