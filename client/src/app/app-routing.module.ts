import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { ContactComponent } from './contact/contact.component';
import { BlogComponent } from './blog/blog.component';
import { CreateblogComponent } from './blog/createblog/createblog.component';
import { ViewBlogComponent } from './blog/viewblog/viewblog.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { UsersComponent } from './users/users.component';
import { CreateEditUserComponent } from './users/createedituser/createedituser.component';
import { CanActivateRouteGuard } from './guards/can-activatre-route.guard';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { VerifyPasswordResetComponent } from './resetpassword/verifypasswordreset/verifypasswordreset.component';
import { ChangePasswordComponent } from './resetpassword/changepassword/changepassword.component';
import { EditProfileComponent } from './profile/editprofile/editprofile.component';

const routes: Routes = [
  { path: 'profile', component: ProfileComponent },
  { path: 'profile/edit', component: EditProfileComponent },
  { path: 'contact', component: ContactComponent },
  { path: '', component: BlogComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'blog/view/:id', component: ViewBlogComponent },
  { path: 'blog/create', component: CreateblogComponent, canActivate: [CanActivateRouteGuard] },
  { path: 'blog/edit', component: CreateblogComponent, canActivate: [CanActivateRouteGuard] },
  { path: 'users', component: UsersComponent, canActivate: [CanActivateRouteGuard] },
  { path: 'users/create', component: CreateEditUserComponent, canActivate: [CanActivateRouteGuard] },
  { path: 'users/edit', component: CreateEditUserComponent, canActivate: [CanActivateRouteGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'reset_password', component: ResetpasswordComponent },
  { path: 'verify-password-reset/:id/:token', component: VerifyPasswordResetComponent },
  { path: 'change-password', component: ChangePasswordComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
