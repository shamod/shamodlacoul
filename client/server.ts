const express = require('express');
const app = express();
// Run the app by serving the static files
// in the dist directory
app.use(express.static(__dirname + '/dist/shamodlacoul'));
// Start the app by listening on the default
// Heroku port

app.get('/.well-known/acme-challenge/:content', function(req, res) {
    res.send('6DtRuLA9O4kbp1oeJV5TZ-IZ388BdITx08SexoTo_7U.gS1LQ9DHS1LCAkGOaZl2wQMg3CVj3ZmM_dwcg_m9Wo8');
});

const server = app.listen(process.env.PORT || 8080, function () {
    const port = server.address().port;
    console.log('App now running on port', port);
});

