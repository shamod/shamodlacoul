export class CreatePostDTO {
    readonly title: string;
    readonly content: string;
    readonly author: string;
    readonly datePosted: string;
}
