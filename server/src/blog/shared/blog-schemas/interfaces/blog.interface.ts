import { Document } from 'mongoose';

export interface Blog extends Document {
    readonly title: string;
    readonly content: string;
    readonly author: string;
    readonly date_posted: string;
}
