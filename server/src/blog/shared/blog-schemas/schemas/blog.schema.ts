import * as mongoose from 'mongoose';

export const BlogSchema = new mongoose.Schema({
    title: String,
    content: String,
    author: String,
    date_posted: String,
});
