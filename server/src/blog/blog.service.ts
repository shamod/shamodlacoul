import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Blog } from './shared/blog-schemas/interfaces/blog.interface';
import { CreatePostDTO } from './shared/blog-schemas/dto/create-post.dto';

@Injectable()
export class BlogService {
    constructor(@InjectModel('Blog') private readonly blogModel: Model<Blog>) {}

    async getPosts(): Promise<Blog[]> {
        const posts = await this.blogModel.find().exec();
        return posts;
    }

    async getPost(postID): Promise<Blog> {
        const blog = await this.blogModel.findById(postID).exec();
        return blog;
    }

    async addPost(createPostDTO: CreatePostDTO): Promise<Blog> {
        const newPost = await this.blogModel(createPostDTO);
        return newPost.save();
    }

    async editPost(postID, createPostDTO: CreatePostDTO): Promise<Blog> {
        const editedPost = await this.blogModel
            .findByIdAndUpdate(postID, createPostDTO, { new: true });
        return editedPost;
    }

    async deletePost(postID): Promise<any> {
        const deletedPost = await this.blogModel
            .findByIdAndRemove(postID);
        return deletedPost;
    }
}
