import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { IUser } from './schemas/interfaces/user.interface';
import { RegistrationStatus } from './schemas/interfaces/registrationstatus.interface';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {

    /* constructor(@InjectModel('User') private readonly userModel: Model<IUser>, public jwtService: JwtService) {}

    async register(user: IUser): Promise<any> {
        let status: RegistrationStatus = { success: true, message: 'user register' };
        await this.userModel.register(new this.userModel({
            username: user.email,
            name: user.name,
        }), user.password, (err) => {
            if (err) {
                status = { success: false, message: err };
            }
        });
        return status;
    }

    createToken(user) {
        const expiresIn = 3600;
        const accessToken = this.jwtService.sign({
            id: user.id,
            email: user.email,
            name: user.name,
        });
        return {
            accessToken,
            expiresIn,
        };
    } */
}
