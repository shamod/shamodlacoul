import { Controller, Post, Body, HttpStatus, Get, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from 'src/users/dtos/createUser.dto';
import { UserService } from 'src/users/users.service';

@Controller('auth')
export class AuthController {

    constructor(private readonly authService: AuthService,
                private readonly userService: UserService) {}

    @Post('register')
    public async register(@Res() res, @Body() createUserDto: CreateUserDto) {
        const result = '';
        if (!result) {
            return res.status(HttpStatus.BAD_REQUEST).json(result);
        }
        return res.status(HttpStatus.OK).json(result);
    }

    @Post('login')
    public async login(@Res() res, @Body() loginUserDto) {
        return await this.userService.findOne({ username: loginUserDto.username}).then(user => {
            if (!user) {
                return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                    message: 'User Not Found',
                });
            }
            const token = '';
            return res.status(HttpStatus.OK).json(token);
        });
    }
}
