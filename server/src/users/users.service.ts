import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser } from './interfaces/user.interface';
import { CreateUserDto } from './dtos/createUser.dto';
import { PasswordService } from './password.service';

@Injectable()
export class UserService {

    constructor(@InjectModel('User') private readonly userModel: Model<IUser>, private readonly passwordService: PasswordService) {}

    async addUser(createUserDTO: CreateUserDto): Promise<any> {
        if (createUserDTO.password) {
            createUserDTO.password = await this.passwordService.hashPassword(createUserDTO.password);
        }
        if (!createUserDTO.status) {
            createUserDTO.status = UserStatus.Pending.toString();
        }
        if (!createUserDTO.roles) {
            createUserDTO.roles = [];
            createUserDTO.roles.push(UserRoles.User.toString());
        }
        if (!createUserDTO.createdAt) {
            createUserDTO.createdAt = Date.now();
        }
        createUserDTO.modifiedAt = Date.now();
        const newUser = await this.userModel(createUserDTO);
        return newUser.save();
    }

    async findOne(options: object): Promise<IUser> {
        return await this.userModel.findOne(options).exec();
    }

    async findById(userId: string): Promise<IUser> {
        return await this.userModel.findById(userId).exec();
    }

    async getUsers() {
        return await this.userModel.find().exec();
    }

    async editUser(userID, createUserDTO: IUser): Promise<any> {
        createUserDTO.modifiedAt = Date.now();
        const editedUser = await this.userModel
            .findByIdAndUpdate(userID, createUserDTO, { new: true });
        return editedUser;
    }

    async deleteUser(userID): Promise<any> {
        const deletedUser = await this.userModel
            .findByIdAndRemove(userID);
        return deletedUser;
    }
}

enum UserStatus {
    Active = 'Active',
    Pending = 'Pending',
    Inactive = 'Inactive',
}

enum UserRoles {
    SuperAdmin = 'SuperAdmin',
    Admin = 'Admin',
    Manager = 'Manager',
    User = 'User',
}
