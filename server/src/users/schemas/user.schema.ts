import * as mongoose from 'mongoose';
import * as passportLocalMongoose from 'passport-local-mongoose';

export const UserSchema = new mongoose.Schema({
    name: String,
    photo: String,
    email: String,
    password: String,
    title: String,
    description: String,
    status: String,
    roles: [String],
    createdAt: Number,
    modifiedAt: Number,
});
