import * as mongoose from 'mongoose';

export const PasswordResetSchema = new mongoose.Schema({
    userId: String,
    token: String,
    status: String,
    createdAt: Number,
    modifiedAt: Number,
});
