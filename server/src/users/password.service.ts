import { Injectable } from '@nestjs/common';
import * as argon2 from 'argon2';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class PasswordService {

    constructor(private readonly jwtService: JwtService) {}

    async hashPassword(password: string) {
        return await argon2.hash(password);
    }

    async verifyPassword(password, loginPassword) {
        return await argon2.verify(password, loginPassword);
    }

    generateJWT(user) {
        return this.jwtService.sign(JSON.stringify(user));
    }
}
