import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IPasswordReset } from './interfaces/passwordreset.interface';

@Injectable()
export class PasswordResetService {

    constructor(@InjectModel('PasswordReset') private readonly passwordResetModel: Model<IPasswordReset>) {}

    async addPasswordReset(passwordResetObject) {
        const newPasswordResetRequest = await this.passwordResetModel(passwordResetObject);
        return newPasswordResetRequest.save();
    }

    async findOne(options: object): Promise<IPasswordReset> {
        return await this.passwordResetModel.findOne(options).exec();
    }
}
