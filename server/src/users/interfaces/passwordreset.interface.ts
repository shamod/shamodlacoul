export interface IPasswordReset {
    readonly _id: string;
    readonly email: string;
    readonly token: string;
}
