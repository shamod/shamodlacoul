import { PassportLocalDocument } from 'mongoose';

export interface IUser extends PassportLocalDocument {
    readonly _id?: string;
    readonly name: string;
    readonly email: string;
    readonly photo: string;
    password: string;
    readonly title: string;
    readonly description: string;
    readonly status: string;
    readonly roles: string[];
    readonly token?: string;
    createdAt: number;
    modifiedAt: number;
}
