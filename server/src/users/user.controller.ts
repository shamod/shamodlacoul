import { Controller, Post, Res, Body, HttpStatus, Param, NotFoundException,
         Get, Put, Query, Delete, UseInterceptors, UploadedFile } from '@nestjs/common';
import { CreateUserDto } from './dtos/createUser.dto';
import { UserService } from './users.service';
import { PasswordService } from './password.service';
import { PasswordResetService } from './passwordReset.service';
import { MessagingService } from 'src/common/messaging.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { async } from 'rxjs/internal/scheduler/async';

@Controller('user')
export class UserController {

    constructor(private readonly userService: UserService,
                private readonly passwordService: PasswordService,
                private readonly passwordResetService: PasswordResetService,
                private readonly messagingService: MessagingService) {}

    @Get('users')
    async getusers(@Res() res) {
        const users = await this.userService.getUsers();
        users.map((item) => {
            item.password = undefined;
        });
        return res.status(HttpStatus.OK).json(users);
    }

    @Get('user/:userId')
    async getUser(@Res() res, @Param('userId') userId) {
        const user = await this.userService.findById(userId);
        if (user.password) { user.password = undefined; }
        if (!user) { throw new NotFoundException('User does not exist!'); }
        return res.status(HttpStatus.OK).json(user);
    }

    @Post('/create')
    async addUser(@Res() res, @Body() createUserDTO: CreateUserDto) {
        if (createUserDTO) {
            const newUser = await this.userService.addUser(createUserDTO);
            return res.status(HttpStatus.OK).json({
                message: 'User has been submitted successfully!',
                user: newUser,
            });
        }
    }

    @Put('/edit')
    async editUser(@Res() res, @Query('userId') userId, @Body() createUserDTO: CreateUserDto) {
        const editedUser = await this.userService.editUser(userId, createUserDTO);
        if (!editedUser) { throw new NotFoundException('User does not exist!'); }
        return res.status(HttpStatus.OK).json({
            message: 'User has been successfully updated',
            user: editedUser,
        });
    }

    @Post('upload')
    @UseInterceptors(FileInterceptor('file',
    {
      storage: diskStorage({
        destination: './photos',
        filename: (req, file, cb) => {
            const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
            return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }))
    async uploadFile(@Res() res, @UploadedFile() file) {
      return res.status(HttpStatus.OK).json({
          filepath: file.path,
        });
    }

    @Get('photos/:fileId')
    async getPhoto(@Res() res, @Param('fileId') fileId) {
        return res.sendFile(fileId, { root: 'photos'});
    }

    @Delete('/delete')
    async deletePost(@Res() res, @Query('userId') userId) {
        const deletedUser = await this.userService.deleteUser(userId);
        if (!deletedUser) { throw new NotFoundException('User does not exist!'); }
        return res.status(HttpStatus.OK).json({
            message: 'User has been deleted!',
            post: deletedUser,
        });
    }

    @Post('login')
    public async login(@Res() res, @Body() loginUserDto) {
        return await this.userService.findOne({ email: loginUserDto.email}).then(async user => {
            if (!user) {
                return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                    message: 'User Not Found',
                });
            }
            const isValidPasswd = await this.passwordService.verifyPassword(user.password, loginUserDto.password);
            if (!isValidPasswd) {
                return res.status(HttpStatus.EXPECTATION_FAILED).json({
                    message: 'Invalid password',
                });
            }
            if (user.password) { user.password = undefined; }
            return res.status(HttpStatus.OK).json({
                name: user.name,
                email: user.email,
                status: user.status,
                roles: user.roles,
                token: this.passwordService.generateJWT(user),
            });
        });
    }

    @Post('password_reset')
    public async resetPassword(@Res() res, @Body() body) {
        return await this.userService.findOne({ email: body.email }).then(async user => {
            const token = this.passwordService.generateJWT(user);
            const passwordResetObject = {
                userId: user._id,
                token,
                status: PasswordResetStats.Pending.valueOf(),
                createdAt: Date.now(),
                modifiedAt: Date.now(),
            };
            this.passwordResetService.addPasswordReset(passwordResetObject);
            const messagingContent = {
                userId: user._id,
                token,
                email: user.email,
            };
            this.messagingService.sendEmail(messagingContent);
            return res.status(HttpStatus.OK).json({
                id: user._id || '',
                token,
            });
        });
    }

    @Post('verify_password_reset')
    public async verifyPasswordResetIdToken(@Res() res, @Body() body) {
        return await this.passwordResetService.findOne({ userId: body.id }).then(async user => {
            if (!user) { throw new NotFoundException('User does not exist!'); }
            if (body.token === user.token) {
                return res.status(HttpStatus.OK).json({
                    verified: true,
                });
            }
        });
    }

    @Put('change-password')
    public async changePassword(@Res() res, @Body() body) {
        return await this.userService.findById(body.userId).then(async user => {
            if (!user) { throw new NotFoundException('User does not exist!'); }
            const isValidPasswd = await this.passwordService.verifyPassword(user.password, body.password);
            if (!isValidPasswd) {
                throw new Error('Old password is invalid');
            }
            user.password = await  this.passwordService.hashPassword(body.password);
            const editedUser = await this.userService.editUser(body.userId, user);
            return res.status(HttpStatus.OK).json({
                message: 'Password has been successfully updated',
                user: editedUser,
            });
        });
    }
}

enum PasswordResetStats {
    Pending = 'pending',
    Verified = 'verified',
    Expired = 'expired',
}
