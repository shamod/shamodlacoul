import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './schemas/user.schema';
import { UserService } from './users.service';
import { UserController } from './user.controller';
import { JwtModule } from '@nestjs/jwt';
import { PasswordService } from './password.service';
import { PasswordResetSchema } from './schemas/password-reset.schema';
import { PasswordResetService } from './passwordReset.service';
import { MessagingService } from 'src/common/messaging.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
        MongooseModule.forFeature([{ name: 'PasswordReset', schema: PasswordResetSchema }]),
        JwtModule.register({
            secret: 'secret',
          })],
    exports: [MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
              MongooseModule.forFeature([{ name: 'PasswordReset', schema: PasswordResetSchema }]),
              UserService,
              PasswordService,
              PasswordResetService,
              MessagingService],
    providers: [UserService,
                PasswordService,
                PasswordResetService,
                MessagingService],
    controllers: [UserController],
})
export class UsersModule {}
