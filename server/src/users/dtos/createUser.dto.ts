export class CreateUserDto {
    readonly id?: number;
    readonly name: string;
    readonly photo: string;
    readonly email: string;
    password: string;
    title: string;
    description: string;
    status: string;
    roles: string[];
    token?: string;
    createdAt: number;
    modifiedAt: number;
}
