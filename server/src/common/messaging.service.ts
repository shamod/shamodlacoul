import { Injectable } from '@nestjs/common';
import { MailerService } from '@nest-modules/mailer';
import * as mailer from 'nodemailer';

@Injectable()
export class MessagingService {

    constructor(private readonly mailerService: MailerService) {}

    sendEmail(content) {

        const smtpTransport = mailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'shamod@gmail.com',
                pass: 'esqdbshspnlifuix',
            },
        });

        smtpTransport
            .sendMail({
              to: content.email,
              from: '"Shamod Lacoul" <shamod@gmail.com>',
              subject: 'Forgot your password verification',
              html: `<p>Hello from us<p><br><a href="http://localhost:4200/verify-password-reset/${content.userId}/${content.token}">Link</a>`,
            }, (error, response) => {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Message sent: ' + response.message);
                }
                smtpTransport.close();
            });
    }
}
